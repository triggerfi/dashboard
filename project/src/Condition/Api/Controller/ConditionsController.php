<?php

namespace App\Conditions\Api\Controller;

use App\Common\Api\Controller\ControllerBase;

final class ConditionsController extends ControllerBase
{
    /*
     * Conditions
     */
    public function listAction(int $eventId): \Phalcon\Http\ResponseInterface
    {
        try {
            $query = new \App\Conditions\Query\GetListOfConditions($eventId);
            $handler = new \App\Conditions\Query\GetListOfConditionsHandler();
            $conditions = $handler->handle($query);
            $this->response->setJsonContent(['data' => $conditions]);
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->response;
    }
}