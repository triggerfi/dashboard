<?php

namespace App\Conditions\Query;

use Models\Factory;

class GetListOfConditionsHandler
{
    public function handle(GetListOfConditions $query): array
    {
        $output = [];

        /** @var \Models\TriggersEventsConditions $model */
        $model = Factory::create('TriggersEventsConditions');
        $data = $model::getByEventId($query->getEventId());

        foreach($data as $v) {
            $output[] = new ConditionView($v->id, $v->name);
        }

        return $output;
    }
}