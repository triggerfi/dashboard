<?php

namespace App\Action\Query;

use Models\Factory;

class GetListOfActionsHandler
{
    public function handle(GetListOfActions $query): array
    {
        $output = [];

        /** @var \Models\TriggersActions $eventsModel */
        $eventsModel = Factory::create('TriggersActions');
        $data = $eventsModel::find();

        foreach($data as $v) {
            $output[] = new ActionView($v->id, $v->name);
        }

        return $output;
    }
}