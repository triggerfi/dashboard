<?php

namespace App\Action\Api\Controller;

use App\Common\Api\Controller\ControllerBase;
use Phalcon\Http\ResponseInterface;

final class ActionController extends ControllerBase
{
    public function listAction(): ResponseInterface
    {
        try {
            $query = new \App\Action\Query\GetListOfActions();
            $handler = new \App\Action\Query\GetListOfActionsHandler();
            $actions = $handler->handle($query);
            $this->response->setJsonContent(['data' => $actions]);
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->response;
    }
}
