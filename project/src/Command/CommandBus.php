<?php


namespace App\Command;

class CommandBus implements CommandHandlerInterface
{
    /**
     * @param \App\Command\CommandInterface $command
     *
     * @throws \App\Command\CommandException
     */
    public function handle(CommandInterface $command): void
    {
        $handlerClassName = get_class($command) . 'Handler';
        if (!class_exists($handlerClassName)) {
            throw new CommandException('Handler for ' . get_class($command) . ' not found');
        }

        /** @var \App\Command\CommandHandlerInterface $handler */
        $handler = new $handlerClassName;
        $handler->handle($command);
    }
}