<?php

namespace App\Command;

interface CommandHandlerInterface
{
    public function handle(CommandInterface $command): void;
}
