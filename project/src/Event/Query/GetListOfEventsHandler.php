<?php


namespace App\Event\Query;

use Models\Factory;
use Phalcon\Di;

final class GetListOfEventsHandler extends Di\Injectable
{
    /**
     * @param \App\Event\Query\GetListOfEvents $query
     *
     * @return \App\Event\Query\EventView[]
     */
    public function handle(GetListOfEvents $query): array
    {
        $output = [];

        /** @var \Models\TriggersEvents $eventsModel */
        $eventsModel = Factory::create('TriggersEvents');
        $data = $eventsModel::find();

        foreach($data as $v) {
            $output[] = new EventView($v->id, $v->name);
        }

        return $output;
    }
}