<?php

namespace App\Event\Api\Controller;

use App\Common\Api\Controller\ControllerBase;

final class EventController extends ControllerBase
{
    /*
     * Events
     */
    public function listAction(): \Phalcon\Http\ResponseInterface
    {
        try {
            $query = new \App\Event\Query\GetListOfEvents();
            $handler = new \App\Event\Query\GetListOfEventsHandler();
            $events = $handler->handle($query);
            $this->response->setJsonContent(['data' => $events]);
        } catch (\Exception $e) {
            return $this->error500($e);
        }

        return $this->response;
    }
}