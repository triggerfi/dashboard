<?php

namespace App\Common\Api\Controller;

use App\Component\Api\Response;
use GuzzleHttp\Client;
use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{

    /**
     * @var \App\Command\CommandBus
     */
    public $commandBus;

    /**
     * @var \App\Component\Api\Response
     */
    public $apiResponse;

    /** @var Client */
    protected $client;

    /*
     * initialize controller
     */
    public function initialize()
    {
        $this->commandBus = new \App\Command\CommandBus();
        $this->apiResponse = new Response();
        $this->client = new Client(['base_uri' => 'http://triggerfi_api/v1/']);

        /*
         * redirect to no access if not logged in
         */
        if (!$this->isAuth()) {
            $controller = $this->router->getControllerName();


            if (!in_array($controller, ['errors', 'auth'])) {
                return $this->response->redirect('/login', true);
            }
        }

        return true;
    }

    public function api(string $method, string $endpoint, $data = null): array
    {
        if (substr($endpoint, 0, 1) === '/') {
            $endpoint = substr($endpoint, 1);
        }

        $options = [];
        if ($data) {
            $options['json'] = $data;
        }

        try {
            $data = $this->client->request($method, $endpoint, $options);
            $data = $data->getBody()->getContents();
        } catch (\Exception $e) {
            return [];
        }

        $data = json_decode($data, true);
        if (isset($data['data'])) {
            return $data['data'];
        }

        return [];
    }

    /*
     * check if call is authenticated
     */
    public function isAuth()
    {
        /**
         * @var \Phalcon\Session\AdapterInterface $session
         */
        $session = $this->di->get('session');
        if ($session->get('is_logged') == true) {
            return true;
        }

        return false;
    }

    /*
     * no access
     */
    public function noAccess(): bool
    {
        return $this->dispatcher->forward([
            'namespace' => '\\App\\UI\\Controller\\Errors',
            'controller' => 'errors',
            'action' => 'forbidden'
        ]);
    }

    /**
     *
     */
    public function notFound()
    {
        return $this->dispatcher->forward([
            'namespace' => '\\App\\UI\\Controller\\Errors',
            'controller' => 'errors',
            'action' => 'notFound'
        ]);
    }

    /**
     * @param \Exception $e
     */
    public function error500(\Exception $e)
    {
        return $this->dispatcher->forward([
            'namespace' => '\\App\\UI\\Controller\\Errors',
            'controller' => 'errors',
            'action' => 'notFound',
            'params' => [
                'exception' => $e
            ]
        ]);
    }

    /**
     * method not allowed
     *
     * @return bool
     */
    public function methodNotAllowed(): bool
    {
        return $this->dispatcher->forward([
            'namespace' => '\\App\\UI\\Controller\\Errors',
            'controller' => 'errors',
            'action' => 'methodNotAllowed'
        ]);
    }

    /**
     * @param int $code
     *
     * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
     */
    public function respond(int $code = 200)
    {
        return $this->response->setStatusCode($code)->setJsonContent($this->apiResponse);
    }
}
