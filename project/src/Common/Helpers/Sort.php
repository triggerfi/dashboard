<?php

namespace App\Common\Helpers;

final class Sort
{
    /*
     * Re-indexes set of data by specific key
     */
    public static function reindexByKey(&$tab, $key, bool $multidimensional = false): bool
    {
        $aNew = [];
        foreach ($tab as $v) {
            if (is_array($v) && isset($v[$key])) {
                if ($multidimensional) {
                    $aNew[$v[$key]][] = $v;
                } else {
                    $aNew[$v[$key]] = $v;
                }
            } elseif (is_object($v) && isset($v->{$key})) {
                if ($multidimensional) {
                    $aNew[$v[$key]][] = $v;
                } else {
                    $aNew[$v->{$key}] = $v;
                }
            }
        }

        $tab = $aNew;
        return true;
    }
}