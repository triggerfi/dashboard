<?php

namespace App\Common\Helpers;

use Phalcon\Exception;
use Phalcon\Security\Random;

final class UUID
{

    protected static $instance;

    public static function generate(): string
    {
        try {
            return (new Random())->uuid();
        } catch (Exception $e) {
            // log shit
            return '';
        }
    }

    public static function fromBin($uuid)
    {
        return preg_replace(
            '/([0-9a-f]{8})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{12})/',
            '$1-$2-$3-$4-$5',
            bin2hex($uuid)
        );
    }

    public static function toBin($uuid): string
    {
        return hex2bin(str_replace('-', '', $uuid));
    }
}
