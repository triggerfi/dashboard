<?php

namespace App\Component\Api;

/**
 * Class Error
 *
 * @link    http://jsonapi.org/format/#errors
 * @package Lib\Api
 */
class Error implements \JsonSerializable
{
    const ERROR_NOT_FOUND = 'Not found';
    const ERROR_INCOMPLETE_DATA = 'Incomplete data';
    const ERROR_INTERNAL = 'Internal error';

    /**
     * @var string
     */
    protected $id;

    /**
     * @var array
     */
    protected $links;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $detail;

    /**
     * @var object
     */
    protected $source;

    /**
     * @var object
     */
    protected $meta;

    /**
     * @throws ApiException
     * @return array
     */
    public function jsonSerialize(): array
    {
        $output = [];

        if ($this->id) {
            $output['id'] = $this->id;
        }

        if ($this->links) {
            $output['links'] = $this->links;
        }

        if ($this->status) {
            $output['status'] = $this->status;
        }

        if ($this->code) {
            $output['code'] = $this->code;
        }

        if ($this->title) {
            $output['title'] = $this->title;
        }

        if ($this->detail) {
            $output['detail'] = $this->detail;
        }

        if ($this->source) {
            $output['source'] = $this->source;
        }

        if ($this->meta) {
            $output['meta'] = $this->meta;
        }

        if (!$output) {
            throw new ApiException('Response can\'t be empty');
        }

        return $output;
    }

    /**
     * Error constructor.
     *
     * @param string $title
     */
    public function __construct(string $title = null)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * @param array $links
     */
    public function setLinks($links)
    {
        $this->links = $links;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * @param string $detail
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;
    }

    /**
     * @return object
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param object $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return object
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param object $meta
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;
    }
}
