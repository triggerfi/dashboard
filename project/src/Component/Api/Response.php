<?php

namespace App\Component\Api;

/**
 * Class Response used to communicate with frontend
 *
 * @link    http://jsonapi.org/format/#document-top-level
 * @package Lib\Api
 */
class Response implements \JsonSerializable
{
    /**
     * @var mixed
     */
    protected $data;

    /**
     * @var Error[]
     */
    protected $errors = [];

    /**
     * @var object
     */
    protected $meta;

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        $output = [];
        if ($this->errors) {
            $output['errors'] = $this->errors;
        }

        if ($this->meta) {
            $output['meta'] = $this->meta;
        }

        if ($this->data && !$this->errors) {
            $output['data'] = $this->data;
        }

        if (!$output) {
            $output['data'] = [];
        }

        return $output;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     *
     * @throws ApiException
     */
    public function setErrors(array $errors)
    {
        foreach ($errors as $v) {
            if (!$v instanceof Error) {
                throw new ApiException('Error should be instance of Api\Error');
            }
        }

        $this->errors = $errors;
    }

    /**
     * @return bool
     */
    public function hasErrors(): bool
    {
        return empty($this->errors) ? false : true;
    }

    /**
     * @param Error $error
     */
    public function setError(Error $error)
    {
        $this->errors[] = $error;
    }

    /**
     * @return object
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @param object $meta
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;
    }
}
