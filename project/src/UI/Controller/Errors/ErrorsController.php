<?php

namespace App\UI\Controller\Errors;

use App\Common\Api\Controller\ControllerBase;

final class ErrorsController extends ControllerBase
{
    /**
     * 404 error page
     */
    public function notFoundAction()
    {
        $this->view->setLayout('blank');
        $this->response->setStatusCode(404, 'Not Found');
        $this->view->setParamToView('title', 'Not Found');
    }

    /**
     * 403 Forbidden
     */
    public function forbiddenAction()
    {
        $this->view->setLayout('blank');
        $this->response->setStatusCode(403, 'Forbidden');
        $this->view->setParamToView('title', 'Forbidden');
    }

    /**
     * 405 Method Not Allowed
     */
    public function methodNotAllowedAction()
    {
        $this->view->setLayout('blank');
        $this->response->setStatusCode(404, 'Method Not Allowed');
        $this->view->setParamToView('title', 'Method Not Allowed');
    }

    /**
     * 500 error page
     */
    public function exceptionAction()
    {
        $this->view->setLayout('blank');
        $this->response->setStatusCode(500, 'Internal server error');
        $this->view->setParamToView('title', 'Internal Server Error');

        /** @var \Exception $e */
        $e = $this->dispatcher->getParam('exception');
        if (!isProd()) {
            $this->view->setParamToView('e', $e);
        }
    }
}
