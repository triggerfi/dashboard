<?php

namespace App\UI\Controller\Automations;

use App\Common\Api\Controller\ControllerBase;

class DatasetsController extends ControllerBase
{
    /*
     * lists actions
     */
    public function listAction()
    {
        $list = $this->api('GET', '/datasets');

        $this->view->setParamToView('datasets', $list);
    }

    public function addAction()
    {
        //
    }

    public function createAction()
    {
        $data = $this->request->getPost();

        $response = $this->api('POST', '/datasets', $data);
        if (empty($response)) {
            return $this->error500(new \Exception('Empty api response'));
        }

        return $this->response->redirect('/automations/variables', true);
    }

    public function deleteAction(string $uuid)
    {
        $response = $this->api('DELETE', '/datasets/' . $uuid);
        if ($response) {
            $this->flash->message('success', 'Variable removed');
        }

        return $this->response->redirect('/automations/variables', true);
    }
}
