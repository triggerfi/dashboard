<?php

namespace App\UI\Controller\Automations;

use App\Common\Api\Controller\ControllerBase;
use App\Common\Helpers\Sort;
use Models\Triggers;
use Models\TriggersVariables;
use Phalcon\Http\ResponseInterface;

class AutomationsController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->setParamToView('list', $this->api('get', '/automations'));
    }

    /*
     * Edit trigger
     */
    public function editAction(string $automationId)
    {
        /** @var Triggers $automation */
        $automation = $this->api('get', '/automations/' . $automationId);

        if (!$automation) {
            return $this->notFound();
        }

        // all actions/events/conditions
        $actions = $this->api('get', '/actions');
        $events = $this->api('get', '/triggers');
        $conditions = $this->api('get', '/conditions');
        $variables = $this->api('get', '/variables');
        $datasets = $this->api('get', '/datasets');

        Sort::reindexByKey($actions, 'id');
        Sort::reindexByKey($events, 'id');
        Sort::reindexByKey($conditions, 'id');
        Sort::reindexByKey($variables, 'uuid');

        $jsonData = \json_decode($automation['dataJson']);
        Sort::reindexByKey($jsonData->conditions, 'id');

        $triggerConditions = $this->api('get', '/triggers/' . $jsonData->event->id . '/conditions');

        $this->view->setParamToView('variables', $variables);
        $this->view->setParamToView('games', []);
        $this->view->setParamToView('actions', $actions);
        $this->view->setParamToView('events', $events);
        $this->view->setParamToView('conditions', $conditions);
        $this->view->setParamToView('triggerConditions', $triggerConditions);
        $this->view->setParamToView('automationConditions', $jsonData->conditions);
        $this->view->setParamToView('automation', $automation);
        $this->view->setParamToView('data', $jsonData);
    }

    /*
     * add trigger sub page
     */
    public function addAction()
    {
        // all actions/events/conditions
        $actions = $this->api('get', '/actions');
        $events = $this->api('get', '/triggers');
        $conditions = $this->api('get', '/conditions');
        $variables = $this->api('get', '/variables');

        Sort::reindexByKey($conditions, 'id');
        Sort::reindexByKey($actions, 'id');
        Sort::reindexByKey($events, 'id');
        Sort::reindexByKey($variables, 'uuid');

        $this->view->setParamToView('actions', $actions);
        $this->view->setParamToView('events', $events);
        $this->view->setParamToView('conditions', $conditions);
        $this->view->setParamToView('games', []);
        $this->view->setParamToView('variables', $variables);
    }

    /*
     * Variables for actions
     */
    public function variablesAction()
    {
        $variables = TriggersVariables::find();
        Sort::reindexByKey($variables, 'id');

        $this->view->setParamToView('variables', $variables);
    }

    /*
     * lists actions
     */
    public function listAction(): ResponseInterface
    {
        $triggers = Triggers::find();
        $this->apiResponse->setData($triggers->toArray());

        return $this->respond();
    }


    /*
     * saves trigger
     */
    public function saveAction(): ResponseInterface
    {
        $data = $this->request->getPost();
        if (empty($data['event']['id'])) {
            throw new \HttpRequestException('Incomplete data');
        }

        //$data = \json_encode($data);
        $response = $this->api('POST', '/automations', $data);
        $this->apiResponse->setData($response);

        return $this->respond();
    }

    /*
     * Returns specific trigger
     */
    public function getAction(int $triggerId): ResponseInterface
    {
        // TODO: remove it
        /*$trigger = Triggers::findFirst($triggerId);
        if (!$trigger) {
            return $this->notFound();
        }

        $this->apiResponse->setData(json_decode($trigger->dataJson));

        return $this->respond();*/
    }
}
