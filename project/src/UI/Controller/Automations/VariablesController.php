<?php

namespace App\UI\Controller\Automations;

use App\Common\Api\Controller\ControllerBase;

class VariablesController extends ControllerBase
{
    /*
     * lists actions
     */
    public function listAction()
    {
        $list = $this->api('GET', '/variables');

        $this->view->setParamToView('variables', $list);
    }

    public function addAction()
    {
        //
    }

    public function createAction()
    {
        $data = $this->request->getPost();

        $response = $this->api('POST', '/variables', $data);
        if (empty($response)) {
            return $this->error500(new \Exception('Empty api response'));
        }

        return $this->response->redirect('/automations/variables', true);
    }

    public function deleteAction(string $uuid)
    {
        $response = $this->api('DELETE', '/variables/' . $uuid);
        $this->flash->message('success', 'Variable removed');
        return $this->response->redirect('/automations/variables', true);
    }
}
