<?php

namespace App\UI\Controller\Authorization;

use App\Command\CommandException;
use App\Common\Api\Controller\ControllerBase;
use App\User\Command\Authenticate;
use App\User\Exception\PasswordIncorrect;
use App\User\Exception\UserNotFound;

final class AuthController extends ControllerBase
{
    public function indexAction()
    {
        $this->view->setLayout('auth');
    }

    public function logoutAction()
    {
        $this->session->destroy(true);

        return $this->response->redirect('/login', true);
    }

    /*
     * authorization process
     */
    public function processAction()
    {
        $email = $this->request->getPost('email');
        $pass = $this->request->getPost('password');

        try {
            $command = new Authenticate($email, $pass);
            $this->commandBus->handle($command);
        } catch (UserNotFound | PasswordIncorrect $e) {
            $this->flash->error('User not found');
            return $this->response->redirect('/login', true);
        } catch (CommandException | \HttpInvalidParamException $e) {
            return $this->error500($e);
        }

        /*
         * All good
         */
        return $this->response->redirect('/', true);
    }
}
