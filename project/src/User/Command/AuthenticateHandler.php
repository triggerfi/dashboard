<?php

namespace App\User\Command;

use App\User\Exception\PasswordIncorrect;
use App\User\Exception\UserNotFound;
use Models\Users;
use Phalcon\Di\Injectable;

final class AuthenticateHandler extends Injectable
{
    public function handle(Authenticate $command): void
    {
        $user = Users::findByEmail($command->getEmail());

        if (!$user) {
            throw new UserNotFound('User not found with that e-mail');
        }

        /*
         * user exists
         * let's check if his password is
         */
        if ($this->security->checkHash($command->getPassword(), $user->password)) {
            $this->session->set('is_logged', true);
            $this->session->set('user', (object)$user->toArray());
            $this->session->set('company', (object)$user->company->toArray());
        } else {
            throw new PasswordIncorrect('Password is incorrect');
        }
    }
}
