<?php

namespace App\User\Command;

use App\Command\CommandInterface;

final class Authenticate implements CommandInterface
{
    private $email;

    private $password;

    public function __construct(string $email, string $password)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new \HttpInvalidParamException('Wrong e-mail format');
        }

        $this->email = $email;
        $this->password = $password;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}
