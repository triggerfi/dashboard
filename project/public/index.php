<?php
//@codingStandardsIgnoreFile
use Phalcon\Di\FactoryDefault;

function isProd()
{
    return (getenv('ENV') == 'production') ? true : false;
}

function pre($s) {echo'<pre>';print_r($s);echo'</pre>';}

error_reporting(0);
ini_set('display_errors', 0);

if (!isProd()) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

try {
    $di = new FactoryDefault();

    include BASE_PATH . '/vendor/autoload.php';
    include BASE_PATH . '/config/services.php';
    include BASE_PATH . '/config/loader.php';
    include BASE_PATH . '/config/router.php';

    $application = new \Phalcon\Mvc\Application($di);

    echo str_replace(["\n", "\r", "\t"], '', $application->handle()->getContent());

} catch (\Exception $e) {
    echo $e->getMessage() . '<br>';
    echo '<pre>' . $e->getTraceAsString() . '</pre>';
}
