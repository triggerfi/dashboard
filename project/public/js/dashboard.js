triggers = {
    getEvents: function () {
        $.get('/triggers/events', function (d) {
            var events = d.data;
            $.each(events, function (k, v) {
                var input = $('<option>' + v.name + '</option>');
                input.val(v.id);

                $('.g-events').append(input);
            });
        });

        $('.g-events').on('change', function () {
            $('.g-conditions').html(' ');
        });
    },

    getActions: function () {
        $.get('/triggers/actions', function (d) {
            var actions = d.data;
            $.each(actions, function (k, v) {
                var input = $('<option>' + v.name + '</option>');
                input.val(v.id);

                $('.g-actions').append(input);
            });
        });
    },

    getConditions: function (eventId) {
        $.get('http://api.gamifyit.local/v1/triggers/' + eventId + '/conditions', function (d) {
            var conditions = d.data;
            triggers.createConditionHtml(conditions);
            triggers.updateApiPreview(conditions);
            return conditions;
        });
    },

    createConditionHtml: function (conditions) {
        var c = $('<p class="g-p-condition"><i class="fa fa-minus-square" aria-hidden="true" style="cursor: pointer"></i> &nbsp; If &nbsp; \n' +
            ' <select name="condition-name" class="form-control mb-2 mb-sm-0 border-input g-conditions-select"><option>-- select --</option></select>\n' +
            ' <select name="condition-sign" class="form-control mb-2 mb-sm-0 border-input g-conditions-signs"><option>-- select --</option></select>\n' +
            ' </p>');

        var input = $('<input name="condition-value" type="text" class="form-control border-input g-conditions-value" placeholder="value"/>');
        var select = $('<select name="condition-value" class="form-control mb-2 mb-sm-0 border-input g-conditions-value"><option>-- select --</option></select>');

        $.each(conditions, function (k, v) {
            var opt = $('<option/>');
            $(opt).text(v.name);
            $(opt).data('signs', JSON.stringify(v.signs));
            $(opt).data('values', JSON.stringify(v.values));
            $(opt).val(v.id);

            $(c).find('.g-conditions-select').append(opt);
        });

        $(c).find('.g-conditions-select').on('change', function () {
            var signs = $(c).find('.g-conditions-select').find('option:selected').data('signs').toString();
            var values = $(c).find('.g-conditions-select').find('option:selected').data('values').toString();
            signs = JSON.parse(signs);
            values = JSON.parse(values);

            $(c).find('.g-conditions-signs').find('option').remove();
            $(signs).each(function (k, v) {
                var o = $('<option>' + v + '</option>');
                o.val(v);
                $(c).find('.g-conditions-signs').append(o);
            });

            // condition values
            $(c).find('.g-conditions-value').remove();
            if (values && (typeof values === 'object')) {
                $.each(values, function (k, v) {
                    var option = $('<option />');
                    option.val(v);
                    option.text(v);
                    $(select).append(option);
                });

                $(c).append(select);
            } else {
                $(c).append(input);
            }

            //triggers.updateApiPreview();
        });

        $(c).find('.fa-minus-square').click(function () {
            $(this).closest('p').remove();
            //triggers.updateApiPreview();
        });

        $('.g-conditions').append(c);
    },

    createFormJson: function () {
        var json = {
            name: null,
            event: {},
            action: {},
            action_details: [],
            condition: 'AND',
            conditions: []
        };

        // event
        $('select[name^=event]').each(function (k, v) {
            json.event = {
                id: parseInt($(this).val())
            }
        });

        if (isNaN(json.event.id)) {
            json.event.id = null;
        }

        // action
        $('select[name^=action\\\[id\\\]]').each(function (k, v) {
            json.action = {
                id: $(this).val()
            }
        });

        // conditions
        $('.g-p-condition').each(function (k, v) {
            var cond = {
                id: parseInt($(this).find('select[name=condition-name]').val()),
                sign: $(this).find('select[name=condition-sign]').val(),
                value: $(this).find('select[name=condition-value], input[name=condition-value]').val()
            };

            json.conditions.push(cond);
        });

        // action details
        $('.g-action-details-' + json.action.id).find('.form-inline, .form').each(function (kk, vv) {
            var details = {};
            $(vv).find('input, select, textarea').each(function (k, v) {
                var attr = $(this).attr('name');
                details[attr] = $(this).val();
            });

            json.action_details.push(details);
        });

        json.name = $('#trigger_name').val();

        return json;
    },

    updateApiPreview: function (conditions) {
        var eventId = $('#tr_event').val();
        $('.g-p-condition').each(function (k, v) {
        });

        var baseUri = 'https://api.triggerfi.com/v1/triggers/' + eventId;
        var post = '{<br />';

        var keys = [];
        $(conditions).each(function (k, v) {
            keys.push(v.name);
        });

        if (keys.length) {
            $('#post_data').show();
        } else {
            $('#post_data').hide();
        }

        function onlyUnique(value, index, self) {
            return self.indexOf(value) === index;
        };

        $(keys.filter(onlyUnique)).each(function (k, v) {
            post += '    "' + v;
            //post += '":"' + $(this).find('select[name=condition-value], input[name=condition-value]').val();
            post += '":"some value"';
            post += '<br />'
        });

        post += '}';

        $('#api1').val(baseUri);
        $('#post_data').html(post);
    },

    save: function () {
        var json = triggers.createFormJson();
        var triggerId = $('#trigger_id').val();
        var url = '/automations/save';

        if (typeof triggerId !== 'undefined') {
            url += '/' + triggerId;
        }

        $.post(url, json, function (d) {
            console.log('RETURN');
            console.log(d);
            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
            if (typeof d.errors === 'undefined') {
                toastr["success"]("Success", "Trigger save");
            } else {
                var msg = '';
                for (var i in d.errors) {
                    msg += d.errors[i].status;
                }
                toastr["error"](msg, "Error");
            }
        })
    }
};

var triggerId = $('#trigger_id').val();
//var url = '/triggers/save';

if (typeof triggerId === 'undefined') {
    //triggers.getEvents();
    //triggers.getActions();
}


$('.g-add-condition').click(function (e) {
    e.preventDefault();
    var id = $(this).closest('.row').find('.g-events').val();
    triggers.getConditions(id);
    return true;
});

$('.g-add-property').click(function (e) {
    e.preventDefault();
    var last = $('.g-property').last();
    $(last).after(last.clone());
    return true;
});

$('.g-actions').on('change', function () {
    var v = parseInt($(this).val());
    $('.g-action-details').hide();
    $('.g-action-details-' + v).show();
    //triggers.updateApiPreview();
});

$('.g-events').on('change', function () {
    $('.g-p-condition').remove();
    triggers.updateApiPreview();
});

datasets = {
    parseInput: function (o) {
        var csv = $(o).val();
        csv = CSVToArray(csv);

        $('#columns_ok').html('');
        for (var i in csv[0]) {
            var btn = $('<button class="btn btn-primary btn-xs" value="1" style="margin:5px 0 0 0;"></button>');
            btn.val(csv[0][i]);
            btn.html(csv[0][i]);
            btn.click(function (e) {
                e.preventDefault();
                $(this).toggleClass('btn-primary');
                $(this).toggleClass('btn-default');
                return false;
            });

            $('#columns_ok').append(btn);
            $('#columns_ok').append(' ');
        }

        datasets.prepareColumns();
    },

    prepareColumns: function () {
        var buttons = $('#columns_ok').find('button.btn-primary');
        var items = [];
        $(buttons).each(function (k, v) {
            if ($(v).text().length > 1) {
                items.push($(v).text());
            }
        });

        $('#columns').val(items.join(','));
    }
};

function CSVToArray(strData, strDelimiter) {
    strDelimiter = (strDelimiter || ",");
    var objPattern = new RegExp(
        (
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
            "([^\"\\" + strDelimiter + "\\r\\n]*))"
        ),
        "gi"
    );

    var arrData = [[]];
    var arrMatches;
    while (arrMatches = objPattern.exec(strData)) {
        var strMatchedDelimiter = arrMatches[1];
        if (
            strMatchedDelimiter.length &&
            (strMatchedDelimiter !== strDelimiter)
        ) {
            arrData.push([]);
        }
        var strMatchedValue;
        if (arrMatches[2]) {
            strMatchedValue = arrMatches[2].replace(
                new RegExp("\"\"", "g"),
                "\""
            );
        } else {
            strMatchedValue = arrMatches[3];
        }
        arrData[arrData.length - 1].push(strMatchedValue);
    }
    return (arrData);
}

