<?php
$router = $di->get('router');

$api = new \Phalcon\Mvc\Router\Group();

/*
 * DASHBOARD
 */
$api->addGet('/', [
    'namespace' => 'App\\UI\\Controller',
    'controller' => 'index',
    'action' => 'index'
]);

/*
 * AUTHORIZATION
 */
$api->addGet('/login', [
    'namespace' => 'App\\UI\\Controller\\Authorization',
    'controller' => 'auth',
    'action' => 'index'
]);

$api->addGet('/logout', [
    'namespace' => 'App\\UI\\Controller\\Authorization',
    'controller' => 'auth',
    'action' => 'logout'
]);

$api->addPost('/login', [
    'namespace' => 'App\\UI\\Controller\\Authorization',
    'controller' => 'auth',
    'action' => 'process'
]);

/*
 * AUTOMATIONS
 */
$api->addGet('/automations', [
    'namespace' => 'App\\UI\\Controller\\Automations',
    'controller' => 'automations',
    'action' => 'index'
]);

$api->addGet('/automations/edit/([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})', [
    'namespace' => 'App\\UI\\Controller\\Automations',
    'controller' => 'automations',
    'action' => 'edit',
    'automationId' => 1
]);

$api->addGet('/automations/add', [
    'namespace' => 'App\\UI\\Controller\\Automations',
    'controller' => 'automations',
    'action' => 'add',
]);

$api->addPost('/automations/save/:params', [
    'namespace' => 'App\\UI\\Controller\\Automations',
    'controller' => 'automations',
    'action' => 'save',
    'params' => 1
]);

// variables
$api->addGet('/automations/variables', [
    'namespace' => 'App\\UI\\Controller\\Automations',
    'controller' => 'variables',
    'action' => 'list',
]);

$api->addGet('/automations/variables/create', [
    'namespace' => 'App\\UI\\Controller\\Automations',
    'controller' => 'variables',
    'action' => 'add',
]);

$api->addPost('/automations/variables/create', [
    'namespace' => 'App\\UI\\Controller\\Automations',
    'controller' => 'variables',
    'action' => 'create',
]);

// datasets
$api->addGet('/automations/datasets', [
    'namespace' => 'App\\UI\\Controller\\Automations',
    'controller' => 'datasets',
    'action' => 'list',
]);

$api->addGet('/automations/datasets/create', [
    'namespace' => 'App\\UI\\Controller\\Automations',
    'controller' => 'datasets',
    'action' => 'add',
]);

$api->addPost('/automations/datasets/create', [
    'namespace' => 'App\\UI\\Controller\\Automations',
    'controller' => 'datasets',
    'action' => 'create',
]);

$api->addGet('/automations/variables/delete/([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})', [
    'namespace' => 'App\\UI\\Controller\\Automations',
    'controller' => 'variables',
    'action' => 'delete',
    'uuid' => 1
]);

/*
 * ERRORS
 */
$api->addGet('/errors/fatal', [
    'namespace' => 'App\\UI\\Controller\\Errors',
    'controller' => 'errors',
    'action' => 'fatal'
]);




/** @var \Phalcon\Mvc\Router $router */
$router->mount($api);
