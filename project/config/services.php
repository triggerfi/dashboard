<?php

use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include BASE_PATH . "/config/config.php";
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () use ($di) {
    $config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $di->set('voltService', function ($view, $di) {
        $volt = new View\Engine\Volt($view, $di);

        $volt->setOptions([
            'compiledPath' => BASE_PATH . '/cache/views/',
            'compiledExtension' => '.compiled',
            'compileAlways' => true
        ]);

        return $volt;
    });

    $config = $di->get('config');
    $view = new \Phalcon\Mvc\View();
    $view->setViewsDir($config->application->viewsDir);
    $view->setRenderLevel(View::LEVEL_LAYOUT);
    $view->setLayout('main');
    $view->setParamToView('session', $di->get('session'));

    $view->registerEngines([
        '.phtml' => 'voltService'
    ]);

    return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $config = $this->getConfig();

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
    $params = [
        'host' => $config->database->host,
        'username' => $config->database->username,
        'password' => $config->database->password,
        'dbname' => $config->database->dbname,
        'charset' => $config->database->charset
    ];

    if ($config->database->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    $connection = new $class($params);

    return $connection;
});


/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

$di->setShared('dispatcher', function () use ($di) {
    $eventsManager = new \Phalcon\Events\Manager();
    $eventsManager->attach('dispatch:beforeException', function ($event, $dispatcher, $exception) use ($di) {
        /**
         * @var \Phalcon\Mvc\Dispatcher $dispatcher
         * @var \Exception              $exception
         * @var \Phalcon\Events\Event   $event
         */
        if ($event->getType() == 'beforeException') {
            switch ($exception->getCode()) {
                // 404 error
                case \Phalcon\Dispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                case \Phalcon\Dispatcher::EXCEPTION_ACTION_NOT_FOUND:
                    $dispatcher->forward([
                        'namespace' => 'App\\UI\\Controller\\Errors',
                        'controller' => 'errors',
                        'action' => 'notFound'
                    ]);

                    return false;

                default:
                    // 500 error
                    $dispatcher->forward([
                        'namespace' => 'App\\UI\\Controller\\Errors',
                        'controller' => 'errors',
                        'action' => 'exception',
                        'params' => [
                            'exception' => $exception
                        ]
                    ]);

                    return false;
            }
        }

        return true;
    });

    $dispatcher = new \Phalcon\Mvc\Dispatcher();
    $dispatcher->setEventsManager($eventsManager);
    return $dispatcher;
});

