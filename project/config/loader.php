<?php

$loader = new \Phalcon\Loader();

// Register some namespaces
$loader->registerNamespaces([
    'App' => __DIR__ . '/../src',
    'Lib' => __DIR__ . '/../app/lib',
    'Models' => __DIR__ . '/../app/models'
]);

$loader->registerDirs([
    $di->get('config')->application->controllersDir,
    $di->get('config')->application->modelsDir
]);

// Register autoloader
$loader->register();
