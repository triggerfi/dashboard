<?php

namespace Models;

final class Factory
{
    public static function create(string $name)
    {
        $modelName = ucfirst($name);
        $class = 'Models\\' . $modelName;
        if (!class_exists($class)) {
            throw new \InvalidArgumentException('Model ' . $name . ' not found');
        }

        return (new $class);
    }
}