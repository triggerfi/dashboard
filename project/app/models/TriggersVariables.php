<?php

namespace Models;

use Phalcon\Text;

final class TriggersVariables extends \Phalcon\Mvc\Model
{
    public $id;

    public $name;

    public $defaultValue;

    public $value;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("gamifyit");
        $this->setSource("triggers_variables");
    }

    public function columnMap()
    {
        $columns = $this->getModelsMetaData()->getAttributes($this);
        $map = [];
        foreach ($columns as $column) {
            $map[$column] = lcfirst(Text::camelize($column));
        }

        return $map;
    }

    public function getSource()
    {
        return 'triggers_variables';
    }
}
