<?php

namespace Models;

/**
 * Class TriggersActions
 */
final class TriggersActions extends \Phalcon\Mvc\Model
{
    public $id;

    public $name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("triggers_actions");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'triggers_actions';
    }
}
