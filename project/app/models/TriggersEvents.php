<?php

namespace Models;

/**
 * Class TriggersEvents
 */
final class TriggersEvents extends \Phalcon\Mvc\Model
{
    public $id;

    public $name;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("triggers_events");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'triggers_events';
    }

}
