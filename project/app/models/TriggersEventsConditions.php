<?php

namespace Models;

use Lib\Helpers\Sort;
use Lib\Trigger\Event\Condition\ConditionFactory;
use Phalcon\Mvc\Model\ResultsetInterface;
use Phalcon\Text;

/**
 * Class TriggersEventsConditions
 */
final class TriggersEventsConditions extends \Phalcon\Mvc\Model
{
    public $id;

    public $eventId;

    public $name;

    public $code;

    public function initialize()
    {
        $this->setSchema("gamifyit");
        $this->setSource("triggers_events_conditions");
        $this->belongsTo('event_id', '\TriggersEvents', 'id', ['alias' => 'TriggersEvents']);
    }

    public function columnMap()
    {
        $columns = $this->getModelsMetaData()->getAttributes($this);
        $map = [];
        foreach ($columns as $column) {
            $map[$column] = lcfirst(Text::camelize($column));
        }

        return $map;
    }

    public function getSource()
    {
        return 'triggers_events_conditions';
    }

    public static function getByEventId(int $eventId): array
    {
        $data = self::find([
            "eventId = :id:",
            "bind" => [
                "id" => $eventId
            ]
        ]);
        $triggerConditions = [];
        foreach ($data as $k => $v) {
            $triggerConditions[] = ConditionFactory::create($v->id, $v->code);
        }

        Sort::reindexByKey($triggerConditions, 'id');

        return $triggerConditions;
    }

}
