<?php

namespace Models;

use Phalcon\Mvc\Model;
use Phalcon\Text;

/**
 * @property \Models\Companies $company
 */
final class Users extends \Phalcon\Mvc\Model
{
    /** @var int */
    public $id;

    /** @var int */
    public $companyId;

    /** @var string */
    public $name;

    /** @var string */
    public $email;

    /** @var string */
    public $password;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("gamifyit");
        $this->setSource("users");
        $this->belongsTo('companyId', 'Models\\Companies', 'id', ['alias' => 'company']);
    }

    public function columnMap()
    {
        $columns = $this->getModelsMetaData()->getAttributes($this);
        $map = [];
        foreach ($columns as $column) {
            $map[$column] = lcfirst(Text::camelize($column));
        }

        return $map;
    }

    /**
     * @param string $email
     *
     * @return \Models\Users|Model
     */
    public static function findByEmail(string $email): Model
    {
        $data = self::findFirst([
            "conditions" => "email = ?1",
            "bind" => [
                1 => $email,
            ]
        ]);

        if ($data) {
            return $data;
        }

        return new self();
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'users';
    }
}
