<?php

namespace Models;

use Phalcon\Text;

final class Triggers extends \Phalcon\Mvc\Model
{
    /**
     * int
     */
    public $id;

    /**
     * @var int
     */
    public $eventId;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $dataJson;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("gamifyit");
        $this->setSource("triggers");
        $this->belongsTo('eventId', '\TriggersEvents', 'id', ['alias' => 'TriggersEvents']);
    }

    public function columnMap()
    {
        $columns = $this->getModelsMetaData()->getAttributes($this);
        $map = [];
        foreach ($columns as $column) {
            $map[$column] = lcfirst(Text::camelize($column));
        }

        return $map;
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'triggers';
    }
}
